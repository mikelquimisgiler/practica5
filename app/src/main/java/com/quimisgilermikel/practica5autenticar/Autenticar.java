package com.quimisgilermikel.practica5autenticar;

import android.app.Activity;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.*;
public class Autenticar extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(Autenticar.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button btnautenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText Cajausuario = (EditText) dialogoLogin.findViewById(R.id.txtUsuario);
                final EditText Cajaclaveusuario = (EditText) dialogoLogin.findViewById(R.id.txtClaveusuario);

                btnautenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Autenticar.this,"Usuario: "+ Cajausuario.getText().toString()+"  Clave: "+Cajaclaveusuario.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                Dialog dialogoRegistrar = new Dialog(Autenticar.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registrar);
                Button btnregistrar=(Button)dialogoRegistrar.findViewById(R.id.btnRegistrar);
                final EditText cajaNombres=(EditText)dialogoRegistrar.findViewById(R.id.txtNombres);
                final EditText cajaApellidos=(EditText)dialogoRegistrar.findViewById(R.id.txtApellidos);

                btnregistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Autenticar.this, "Nombres: " + cajaNombres.getText().toString() + "  Apellidos: " + cajaApellidos.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
                dialogoRegistrar.show();
                break;

        }

        return true;
    }

}

